# Zigbee2MQTT tools

[![pipeline status](https://gitlab.com/rubdos/z2m-tools/badges/master/pipeline.svg)](https://gitlab.com/rubdos/z2m-tools/-/commits/master) [![coverage report](https://gitlab.com/rubdos/z2m-tools/badges/master/coverage.svg)](https://gitlab.com/rubdos/z2m-tools/-/commits/master)


[Zigbee2MQTT](https://www.zigbee2mqtt.io/) is a piece of software that links Zigbee hardware to an MQTT server.
It allows to talk to [many devices](https://www.zigbee2mqtt.io/information/supported_devices.html),
and then interconnect them with for example [Home Assistant](https://www.home-assistant.io/), [OpenHAB](https://www.openhab.org/) or [Node-RED](https://nodered.org/).

Zigbee2MQTT is a very powerful tool, but can be painful to configure and manage.
This repository is home to the `z2m` executable,
which aims to be an interactive configuration tool for Zigbee2MQTT.

## Features

- List devices (`z2m device list`) and their last-seen date and time (`-s`/`--last-seen`), optionally filtered by group (`-g`).
  Example output:
    ```
    MAC address         Friendly name           Last Seen
    -----------         -----------             -----------
    0x00124b0007687e6a  Coordinator             2020-03-01 19:17:08.467 UTC
    0x086bd7fffe413ee9  AquariumControlOutlet   2020-03-01 19:16:33.721 UTC
    0x000d6ffffe7acc67  IKEARepeater            2020-03-01 19:17:01.893 UTC
    0x086bd7fffe42b74a  SpeakerControlOutlet    2020-03-01 19:17:03.883 UTC
    0x14b457fffe368440  NightLight              2020-03-01 19:17:05.961 UTC
    0x14b457fffe45b334  LEDSalonSpot2           2020-03-01 19:17:07.849 UTC
    0x086bd7fffe5fa460  TradfriSwitch0          2020-03-01 19:11:12.540 UTC
    0x000d6ffffe7c8653  TradfriSwitch1          2020-03-01 19:11:14.816 UTC
    ```
- List groups (`z2m group list`)
    ```
    ID     Friendly name   Device count
    -----  -----           -----
    1      SalonSpots      3
    2      SalonBulbs      3
    3      Salon           6
    4      SpectrumLamps   7
    5      BatteryPowered  5
    ```
    - Add devices to groups (`z2m group add <group> <device>`)
    - Remove devices from groups (`z2m group remove <group> <device>`)
- Set a property (change brightness/color/...) for groups and devices (`z2m group|device set <group/device> <property> <value>`)
- Identify devices, for example useful for sites with many lamps (`z2m device identify <device> [-l|--long-blink]`).
- Check for and apply OTA updates (`z2m device ota-update [device] [--update]`).
  Example output (`z2m device ota-update -U PianoSwitch`)
    ```
    Update available for PianoSwitch
    Update progress of PianoSwitch: 0% (Update of 'PianoSwitch' at 0%)
    Update progress of PianoSwitch: 0.63% (Update of 'PianoSwitch' at 0.63%, +- 111 minutes remaining)
    Update progress of PianoSwitch: 1.23% (Update of 'PianoSwitch' at 1.23%, +- 99 minutes remaining)
    Update progress of PianoSwitch: 1.79% (Update of 'PianoSwitch' at 1.79%, +- 95 minutes remaining)
    Update progress of PianoSwitch: 2.45% (Update of 'PianoSwitch' at 2.45%, +- 89 minutes remaining)
    Update progress of PianoSwitch: 3.01% (Update of 'PianoSwitch' at 3.01%, +- 88 minutes remaining)
    Update progress of PianoSwitch: 3.68% (Update of 'PianoSwitch' at 3.68%, +- 85 minutes remaining)
    ```
  If the executable crashes half-way, it can be restarted and the progress will resume printing.

Planned features are listed on the [1.0.0 milestone](https://gitlab.com/rubdos/z2m-tools/-/milestones/1) and the [issue tracker](https://gitlab.com/rubdos/z2m-tools/issues?label_name=enhancement).

## Installation

Pre-built binaries are available from Gitlab CI for Linux 64-bit from the [releases page](https://gitlab.com/rubdos/z2m-tools/-/releases).
Every commit is also automatically built and stored:
  - Built with stable Rust: https://gitlab.com/rubdos/z2m-tools/-/jobs/artifacts/master/browse?job=rust-stable-build
  - Built with nightly Rust: https://gitlab.com/rubdos/z2m-tools/-/jobs/artifacts/master/browse?job=rust-nightly-build

[It is unknown whether we can build for Windows yet](https://gitlab.com/rubdos/z2m-tools/issues/7).
Building on macOS should be possible from source.

To build and install from source, install [Rust](https://rust-lang.org/),
for example via [rustup](https://rustup.rs/),
and simply run

```
cargo install --git https://gitlab.com/rubdos/z2m-tools
```

## License

z2m-tools
Copyright (C) 2020  Ruben De Smet

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

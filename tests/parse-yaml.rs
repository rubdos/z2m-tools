#[test]
fn parse_z2m_yaml() {
    let yml = clap::YamlLoader::load_from_str(include_str!("../src/bin/z2m/cli.yaml")).unwrap();

    println!("{:?}", yml);

    clap::App::from(&yml[0]);
}

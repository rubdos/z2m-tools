use std::io::{self, Write};

use failure::*;
use tabwriter::*;
use z2m_tools::Zigbee2Mqtt;

dispatch! {
    "list" -> list_devices,
    "set" -> set_property,
    "identify" -> identify,
    "rename" -> rename,
    "rename-last" -> rename_last,
    "ota-update" -> check_update,
}

pub fn list_devices<T: AsRef<str>>(
    _matches: &clap::ArgMatches,
    submatches: &clap::ArgMatches,
    conn: &mut Zigbee2Mqtt<T>,
) -> Result<(), Error> {
    let mut tw = TabWriter::new(vec![]);

    write!(&mut tw, "MAC address\tFriendly name")?;
    let mut cols = 2;

    if submatches.is_present("device-type") {
        write!(&mut tw, "\tDevice type")?;
        cols += 1;
    }
    if submatches.is_present("last-seen") {
        write!(&mut tw, "\tLast Seen")?;
        cols += 1;
    }

    writeln!(&mut tw)?;

    write!(&mut tw, "-----------")?;
    for _ in 1..cols {
        write!(&mut tw, "\t-----------")?;
    }
    writeln!(&mut tw)?;

    let group = if let Some(group_name) = submatches.value_of("group") {
        let groups = conn.list_groups()?;
        let group = groups
            .into_iter()
            .filter(|g| g.friendly_name == group_name)
            .next();
        ensure!(group.is_some(), "Group {} does not exist.", group_name);
        group
    } else {
        None
    };

    for i in conn.list_devices()? {
        if let Some(group) = group.as_ref() {
            if !group.devices.contains(&i.ieee_addr) {
                continue;
            }
        }

        write!(&mut tw, "{}\t{}", i.ieee_addr, i.friendly_name)?;

        if submatches.is_present("device-type") {
            write!(&mut tw, "\t{}", i.zigbee_type)?;
        }
        if submatches.is_present("last-seen") {
            match i.last_seen {
                Some(last_seen) => write!(&mut tw, "\t{}", last_seen)?,
                None => write!(&mut tw, "\tUnknown")?,
            }
        }

        writeln!(&mut tw)?;
    }

    tw.flush()?;
    write!(
        io::stdout(),
        "{}",
        String::from_utf8(tw.into_inner().unwrap()).unwrap()
    )?;

    io::stdout().flush()?;

    Ok(())
}

fn set_property<T: AsRef<str>>(
    _matches: &clap::ArgMatches,
    submatches: &clap::ArgMatches,
    conn: &mut Zigbee2Mqtt<T>,
) -> Result<(), Error> {
    let device = submatches.value_of("device").unwrap();
    let property = submatches.value_of("property").unwrap();
    let value = submatches.value_of("value").unwrap();

    conn.set_property(device, property, value)?;

    Ok(())
}

fn rename<T: AsRef<str>>(
    _matches: &clap::ArgMatches,
    submatches: &clap::ArgMatches,
    conn: &mut Zigbee2Mqtt<T>,
) -> Result<(), Error> {
    let new = submatches.value_of("new").unwrap();
    let old = submatches.value_of("old").unwrap();

    conn.rename(old, new)?;

    Ok(())
}

fn rename_last<T: AsRef<str>>(
    _matches: &clap::ArgMatches,
    submatches: &clap::ArgMatches,
    conn: &mut Zigbee2Mqtt<T>,
) -> Result<(), Error> {
    let device = submatches.value_of("device").unwrap();

    conn.rename_last(device)?;

    Ok(())
}

fn identify<T: AsRef<str>>(
    _matches: &clap::ArgMatches,
    submatches: &clap::ArgMatches,
    conn: &mut Zigbee2Mqtt<T>,
) -> Result<(), Error> {
    let device = submatches.value_of("device").unwrap();

    let val = if submatches.is_present("long") {
        "lselect"
    } else {
        "select"
    };
    conn.set_property(device, "alert", val)?;

    Ok(())
}

fn check_update<T: AsRef<str>>(
    _matches: &clap::ArgMatches,
    submatches: &clap::ArgMatches,
    conn: &mut Zigbee2Mqtt<T>,
) -> Result<(), Error> {
    let devices = if let Some(device) = submatches.value_of("device") {
        vec![device.to_string()]
    } else {
        conn.list_devices()?
            .into_iter()
            .map(|d| d.friendly_name)
            .collect()
    };

    let do_update = submatches.is_present("update");

    for device in &devices {
        match conn.check_update(device) {
            Ok(true) => {
                println!("Update available for {}", device);
                if do_update {
                    conn.do_update(device)?;
                }
            }
            Ok(false) => println!("No update available for {}", device),
            Err(e) => println!("Error occured for {}: {}", device, e),
        }
    }

    Ok(())
}

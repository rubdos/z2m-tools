use clap::App;
use failure::*;
use paho_mqtt::client::Client;
use z2m_tools::Zigbee2Mqtt;

#[macro_export]
macro_rules! dispatch {
    ($($name:literal -> $method:path,)+) => {
        pub fn main<T: AsRef<str>>(
            matches: &clap::ArgMatches,
            conn: &mut Zigbee2Mqtt<T>,
        ) -> Result<(), Error> {
            let name = match matches.subcommand_name() {
                None => bail!("Please specify a subcommand."),
                Some(name) => name,
            };
            let submatches = matches.subcommand_matches(name);
            match name {
                $($name => $method(matches, &submatches.unwrap(), conn),)+
                _ => bail!("No subcommand {}", name),
            }
        }
    }
}

mod device;
mod group;

fn main() -> Result<(), Error> {
    env_logger::init();

    let manifest = cargo_toml::Manifest::from_str(include_str!("../../../Cargo.toml")).unwrap();
    let package = manifest.package.unwrap();

    let yaml = clap::load_yaml!("cli.yaml");
    let matches = App::from_yaml(yaml)
        .version(&package.version as &str)
        .author(&(package.authors.join(" and ")) as &str)
        .about(&package.description.unwrap() as &str)
        .get_matches();

    let uri = matches.value_of("uri").unwrap();
    let username = matches.value_of("username").unwrap();
    let password = matches.value_of("password").unwrap();

    let client = Client::new(
        paho_mqtt::CreateOptionsBuilder::new()
            .server_uri(uri)
            .client_id("z2m-tools")
            .finalize(),
    )?;
    client.connect(
        paho_mqtt::ConnectOptionsBuilder::new()
            .user_name(username)
            .password(password)
            .finalize(),
    )?;

    let mut conn = Zigbee2Mqtt::new(client, "zigbee2mqtt");

    match matches.subcommand_name() {
        None => bail!("No subcommand given"),
        Some(name) => {
            let submatches = matches.subcommand_matches(name).unwrap();
            match name {
                "device" => device::main(&submatches, &mut conn),
                "group" => group::main(&submatches, &mut conn),
                x => bail!("Unknown subcommand {}", x),
            }
        }
    }
}

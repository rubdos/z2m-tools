use std::io::{self, Write};

use failure::*;
use tabwriter::*;
use z2m_tools::Zigbee2Mqtt;

dispatch! {
    "list" -> list_groups,
    "add" -> add_device,
    "remove" -> remove_device,
    "set" -> set_property,
    "show" -> crate::device::list_devices,
}

fn add_device<T: AsRef<str>>(
    _matches: &clap::ArgMatches,
    submatches: &clap::ArgMatches,
    conn: &mut Zigbee2Mqtt<T>,
) -> Result<(), Error> {
    let group = submatches.value_of("group").unwrap();
    let device = submatches.value_of("device").unwrap();

    conn.add_to_group(group, device)?;

    Ok(())
}

fn remove_device<T: AsRef<str>>(
    _matches: &clap::ArgMatches,
    submatches: &clap::ArgMatches,
    conn: &mut Zigbee2Mqtt<T>,
) -> Result<(), Error> {
    let group = submatches.value_of("group").unwrap();
    let device = submatches.value_of("device").unwrap();

    conn.remove_from_group(group, device)?;

    Ok(())
}

fn list_groups<T: AsRef<str>>(
    _matches: &clap::ArgMatches,
    _submatches: &clap::ArgMatches,
    conn: &mut Zigbee2Mqtt<T>,
) -> Result<(), Error> {
    let mut tw = TabWriter::new(vec![]);

    write!(&mut tw, "ID\tFriendly name\tDevice count")?;
    let cols = 3;

    writeln!(&mut tw)?;

    write!(&mut tw, "-----")?;
    for _ in 1..cols {
        write!(&mut tw, "\t-----")?;
    }
    writeln!(&mut tw)?;

    for group in conn.list_groups()? {
        write!(
            &mut tw,
            "{}\t{}\t{}",
            group.id,
            group.friendly_name,
            group.devices.len()
        )?;

        writeln!(&mut tw)?;
    }

    tw.flush()?;
    write!(
        io::stdout(),
        "{}",
        String::from_utf8(tw.into_inner().unwrap()).unwrap()
    )?;

    io::stdout().flush()?;

    Ok(())
}

fn set_property<T: AsRef<str>>(
    _matches: &clap::ArgMatches,
    submatches: &clap::ArgMatches,
    conn: &mut Zigbee2Mqtt<T>,
) -> Result<(), Error> {
    let group = submatches.value_of("group").unwrap();
    let property = submatches.value_of("property").unwrap();
    let value = submatches.value_of("value").unwrap();

    conn.set_property(group, property, value)?;

    Ok(())
}

use std::sync::mpsc::Receiver;

use chrono::prelude::*;
use failure::*;
use log::*;

use paho_mqtt::client::Client;
use paho_mqtt::message::Message;

pub struct Zigbee2Mqtt<T> {
    base_topic: T,
    client: Client,
    receiver: Receiver<Option<Message>>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
#[serde(rename_all = "camelCase")]
pub struct LogMeta {
    status: String,
    device: String,
    progress: Option<f32>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Zigbee2MqttLogItem<T> {
    #[serde(rename = "type")]
    type_: String,
    message: T,
    meta: Option<LogMeta>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ZigbeeDevice {
    pub ieee_addr: String,
    #[serde(rename = "type")]
    pub zigbee_type: String,
    #[serde(rename = "friendly_name")]
    pub friendly_name: String,
    pub network_address: usize,
    #[serde(with = "chrono::serde::ts_milliseconds_option")]
    pub last_seen: Option<DateTime<Utc>>,
    pub model: Option<String>,
    pub manufacturer_id: Option<usize>,
    pub manufacturer_name: Option<String>,
    pub power_source: Option<String>,
    pub model_id: Option<String>,
    pub hardware_version: Option<usize>,
    pub software_build_id: Option<String>,
    pub date_code: Option<String>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ZigbeeGroup {
    pub optimistic: bool,
    pub devices: Vec<String>,
    #[serde(rename = "friendly_name")]
    pub friendly_name: String,
    pub transition: Option<usize>,
    pub retain: Option<bool>,
    #[serde(rename = "ID")]
    pub id: usize,
}

impl<T: AsRef<str>> Zigbee2Mqtt<T> {
    pub fn new(mut client: Client, base_topic: T) -> Self {
        Self {
            receiver: client.start_consuming(),
            client,
            base_topic,
        }
    }

    pub fn do_update<D: AsRef<str>>(&self, device: D) -> Result<(), Error> {
        let device = device.as_ref();

        let pub_topic = format!("{}/bridge/ota_update/update", self.base_topic.as_ref());

        let msg = self.watch_log("ota_update", |client| {
            client.publish(Message::new(pub_topic, device, 2))?;
            Ok(())
        })?;

        let meta = msg.meta.expect("Meta for ota_update");
        match meta.status.as_ref() {
            "update_in_progress" => (),
            "update_progress" => {
                println!(
                    "Update already in progress for {}: {}% ({})",
                    device,
                    meta.progress.unwrap(),
                    msg.message
                );
            }
            _ => bail!(
                "Unrecognised response {} ({}) for device {}",
                meta.status,
                msg.message,
                device
            ),
        }

        loop {
            let msg = self.watch_log("ota_update", |_| Ok(()))?;

            let meta = msg.meta.expect("Meta for ota_update");
            match meta.status.as_ref() {
                "update_progress" => {
                    println!(
                        "Update progress of {}: {}% ({})",
                        device,
                        meta.progress.unwrap(),
                        msg.message
                    );
                    // Since we synchronously subscribe,
                    // we can miss the `update_succeeded` message.
                    // cfr. https://gitlab.com/rubdos/z2m-tools/issues/6
                    if meta.progress.unwrap() == 100. {
                        return Ok(());
                    }
                }
                "available" => (), // no-op
                "update_failed" => bail!("Update failed for {}: {}", device, msg.message),
                "update_succeeded" => {
                    break;
                }
                _ => bail!("Unrecognised status {} for device {}", meta.status, device),
            }
        }

        Ok(())
    }

    pub fn rename<D: AsRef<str>>(&self, old_name: D, device: D) -> Result<(), Error> {
        let pub_topic = format!("{}/bridge/config/rename", self.base_topic.as_ref());

        let json = serde_json::json!({ "old": old_name.as_ref(), "new": device.as_ref() });
        self.client
            .publish(Message::new(pub_topic, json.to_string(), 2))?;

        Ok(())
    }

    pub fn rename_last<D: AsRef<str>>(&self, device: D) -> Result<(), Error> {
        let pub_topic = format!("{}/bridge/config/rename_last", self.base_topic.as_ref());
        self.client
            .publish(Message::new(pub_topic, device.as_ref(), 2))?;

        Ok(())
    }

    pub fn check_update<D: AsRef<str>>(&self, device: D) -> Result<bool, Error> {
        let device = device.as_ref();

        let pub_topic = format!("{}/bridge/ota_update/check", self.base_topic.as_ref());

        let msg = self.watch_log("ota_update", |client| {
            client.publish(Message::new(pub_topic, device, 2))?;
            Ok(())
        })?;

        let meta = msg.meta.expect("Meta for ota_update");
        match meta.status.as_ref() {
            "checking_if_available" => (),
            "update_progress" => {
                // Update already in progress
                return Ok(true);
            }
            "not_supported" => bail!("Device {} does not support OTA updates.", device),
            _ => bail!(
                "Unrecognised response {} ({}) for device {}",
                meta.status,
                msg.message,
                device
            ),
        }

        let msg = self.watch_log("ota_update", |_| Ok(()))?;

        let meta = msg.meta.expect("Meta for ota_update");
        Ok(match meta.status.as_ref() {
            "not_available" => false,
            "available" => true,
            "check_failed" => bail!("{}", msg.message),
            _ => bail!("Unrecognised status {} for device {}", meta.status, device),
        })
    }

    /// Set property of a group or device.
    pub fn set_property<A, B, C>(&self, device: A, property: B, value: C) -> Result<(), Error>
    where
        A: AsRef<str>,
        B: AsRef<str>,
        C: AsRef<str>,
    {
        let device = device.as_ref();
        let property = property.as_ref();
        let value = value.as_ref();

        let pub_topic = format!("{}/{}/set", self.base_topic.as_ref(), device);
        let json = serde_json::json!({ property: value });

        self.client
            .publish(Message::new(pub_topic, json.to_string(), 2))?;

        Ok(())
    }

    pub fn list_groups(&self) -> Result<Vec<ZigbeeGroup>, Error> {
        let msg = self.trigger_and_get_first("bridge/config/groups", "bridge/log")?;
        let payload = msg.payload_str();
        debug!("list-groups got {}", payload);
        let groups: Zigbee2MqttLogItem<_> = serde_json::from_str(&payload)?;
        Ok(groups.message)
    }

    pub fn add_to_group(
        &self,
        group: impl AsRef<str>,
        device: impl AsRef<str>,
    ) -> Result<(), Error> {
        // zigbee2mqtt/bridge/group/[GROUP_FRIENDLY_NAME]/add
        let pub_topic = format!(
            "{}/bridge/group/{}/add",
            self.base_topic.as_ref(),
            group.as_ref()
        );

        self.client
            .publish(Message::new(pub_topic, device.as_ref(), 2))?;

        Ok(())
    }

    pub fn remove_from_group(
        &self,
        group: impl AsRef<str>,
        device: impl AsRef<str>,
    ) -> Result<(), Error> {
        // zigbee2mqtt/bridge/group/[GROUP_FRIENDLY_NAME]/remove
        let pub_topic = format!(
            "{}/bridge/group/{}/remove",
            self.base_topic.as_ref(),
            group.as_ref()
        );

        self.client
            .publish(Message::new(pub_topic, device.as_ref(), 2))?;

        Ok(())
    }

    pub fn list_devices(&self) -> Result<Vec<ZigbeeDevice>, Error> {
        let msg =
            self.trigger_and_get_first("bridge/config/devices/get", "bridge/config/devices")?;
        let payload = msg.payload_str();
        debug!("list-devices got {}", payload);
        let devices = serde_json::from_str(&payload)?;
        Ok(devices)
    }

    // XXX: implement this in a streaming way.
    fn watch_log(
        &self,
        log_type: &str,
        action: impl FnOnce(&Client) -> Result<(), Error>,
    ) -> Result<Zigbee2MqttLogItem<String>, Error> {
        // '{"type":"ota_update","message":"Device 'Coordinator' does not support OTA updates","meta":{"status":"not_supported","device":"Coordinator"}}'
        let message = self.subscribe_and_filter("bridge/log", action, |msg: &Message| {
            let payload = msg.payload_str();
            let msg: Zigbee2MqttLogItem<&str> = match serde_json::from_str(&payload) {
                Ok(msg) => msg,
                Err(_e) => return false,
            };
            msg.type_ == log_type
        })?;

        let msg = serde_json::from_str(&message.payload_str())?;
        Ok(msg)
    }

    /// Executes the Action `f` once while subscribed to the topic `sub_topic`, then filters that
    /// topic with the supplied Filter `filter`.
    fn subscribe_and_filter<Action, Filter>(
        &self,
        sub_topic: &str,
        f: Action,
        mut filter: Filter,
    ) -> Result<Message, Error>
    where
        Action: FnOnce(&Client) -> Result<(), Error>,
        Filter: FnMut(&Message) -> bool,
    {
        let sub_topic = format!("{}/{}", self.base_topic.as_ref(), sub_topic);
        self.client.subscribe(&sub_topic, 2).unwrap();

        f(&self.client)?;

        let message = loop {
            let message = match self.receiver.recv()? {
                Some(msg) => msg,
                None => bail!("None response"),
            };
            if filter(&message) {
                break message;
            }
        };

        self.client.unsubscribe(&sub_topic)?;

        Ok(message)
    }

    fn trigger_and_get_first(
        &self,
        trigger_topic: &str,
        sub_topic: &str,
    ) -> Result<Message, Error> {
        self.subscribe_and_filter(
            sub_topic,
            |client| {
                client.publish(Message::new(
                    &format!("{}/{}", self.base_topic.as_ref(), trigger_topic),
                    "",
                    2,
                ))?;
                Ok(())
            },
            |msg: &Message| msg.topic().ends_with(sub_topic),
        )
    }
}
